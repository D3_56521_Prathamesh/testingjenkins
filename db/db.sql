create table Movie(
    movie_id INT primary key auto_increment,
    movie_title VARCHAR(30),
    movie_release_date Date,
    movie_time TIME,
    director_name VARCHAR(30)
);

