const mysql = require('mysql2')

const pool = mysql.createPool({

    host: 'sqlnewcont',
    user: 'root',
    password: 'manager',
    database : 'mydb',
    port: 3306,
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0
})

module.exports = pool