const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.get('/:movie_title',(request,response)=>{
    const { movie_title } = request.params
    const query = `select movie_id, movie_title, movie_release_date, movie_time, director_name 
    From Movie where movie_title = '${movie_title}'`

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.post('/',(request,response)=>{
    const { movie_title, movie_release_date, movie_time, director_name } = request.body
    const query = `insert into Movie (movie_title, movie_release_date, movie_time, director_name ) values ('${movie_title}', '${movie_release_date}', '${movie_time}', '${director_name}')`

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.put('/:movie_id',(request,response)=>{
    const { movie_id } = request.params
    const {  movie_release_date, movie_time} = request.body

    const query = `update Movie 
                    set 
                        movie_release_date = '${movie_release_date}', 
                        movie_time = '${movie_time}' 
                        where 
                        movie_id = ${movie_id}`

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.delete('/:movie_id',(request,response)=>{
    const { movie_id } = request.params

    const query = `Delete from Movie 
                    where 
                    movie_id = ${movie_id}`

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

module.exports = router