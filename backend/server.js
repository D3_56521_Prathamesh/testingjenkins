const express = require('express')
const cors = require('cors')

const app = express()
app.use(cors('*'))
app.use(express.json())


const routerMovie = require('./routes/movie')
app.use('/movie',routerMovie)

app.get('/',(request,response)=>{
    response.send(`Connected to back end server`)
})

app.listen(4000, '0.0.0.0', ()=>{
    console.log('Back end started on port 4000')
})